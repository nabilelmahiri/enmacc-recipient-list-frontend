import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import {
  RecipientApi,
  RecipientList,
  Company
} from "../services/recipient-api.service";
import { startWith, map } from "rxjs/operators";
import { MatAutocompleteSelectedEvent } from "@angular/material";

@Component({
  selector: "app-recipients",
  templateUrl: "./recipients.component.html",
  styleUrls: ["./recipients.component.scss"]
})
export class RecipientsComponent implements OnInit {
  searchRecipientsControl = new FormControl();
  option: any = [];
  companies: Company[];
  recipientLists: RecipientList[];
  checkboxsList: any = [];
  filteredOptions: any = [];
  hideAdd = false;
  selectedEmails: any = [];
  selectedCompanies: any = [];

  constructor(private RecipientService: RecipientApi) {
    this.getCompanies();
    this.getRecipientLists();
  }

  ngOnInit() {
    this.filteredOptions = this.searchRecipientsControl.valueChanges.pipe(
      startWith(),
      map(value => (typeof value === "string" ? value : value.name)),
      map(name => this._filter(name))
    );
  }

  displayFn(result?: any): string | undefined {
    return result ? result.name : undefined;
  }

  private _filter(value: string) {
    const filterValue = value.toLowerCase();

    // handel the emails suggestions
    if (filterValue.indexOf("@") > 0) {
      // if it detect an email
      let emailTmp: any = [];
      this.recipientLists.forEach(function(recipientList) {
        emailTmp.push(
          recipientList.emailRecipients.filter(
            option => option.toLowerCase().indexOf(filterValue) === 0
          )
        );
      });
      emailTmp = emailTmp.flat(1); // flateen the array
      emailTmp = emailTmp.filter((el, i, a) => i === a.indexOf(el)); // remove duplicates

      // remove from suggestions
      if (this.selectedEmails.length > 0) {
        let tmp: any = [];
        this.selectedEmails.forEach(function(se) {
          se.emails.forEach(function(sen) {
            tmp.push(sen.name);
          });
        });
        emailTmp = this.getUnique(emailTmp, tmp);
      }

      emailTmp.forEach(function(recipientList, i) {
        emailTmp[i] = { id: i, name: recipientList }; // build the email object
      });

      if (emailTmp.length > 0) {
        return emailTmp;
      }

      // if it detecte new email
      if (this.validateEmail(filterValue)) {
        this.hideAdd = true;
      } else {
        this.hideAdd = false;
      }
    }

    // handel the companies suggestions
    let companyTmp: any = [];
    if (this.selectedCompanies.length !== 0) {
      companyTmp = this.companies.filter(
        option => option.name.toLowerCase().indexOf(filterValue) === 0
      );

      companyTmp = this.getUnique(companyTmp, this.selectedCompanies); // remove from suggestions
      return companyTmp;
    }

    return this.companies.filter(
      option => option.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  // get companies from api service
  getCompanies(): void {
    this.RecipientService.getCompanies().subscribe(
      resultArray => (this.companies = resultArray),
      error => console.log("Error :: " + error)
    );
  }

  // get recipients lists from api service
  getRecipientLists(): void {
    this.RecipientService.getRecipientLists().subscribe(
      resultArray => (this.recipientLists = resultArray),
      error => console.log("Error :: " + error)
    );
  }

  // triggered when the user select a suggestion
  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    let selectedvalue = event.option.value;
    if (this.validateEmail(selectedvalue.name)) {
      let domain = this.getDomain(selectedvalue.name);
      let index = this.checkDomain(this.selectedEmails, domain);
      if (index !== undefined) {
        this.selectedEmails[index].emails.push(selectedvalue);
      } else {
        this.selectedEmails.push({
          domain: domain,
          emails: [selectedvalue]
        });
      }
    } else {
      this.selectedCompanies.push(selectedvalue);
    }
    this.searchRecipientsControl.setValue(""); // empty the input field
  }

  // display the recipients Lists
  checkList(e, index, element) {
    const self = this;
    this.selectedCompanies = [];
    this.selectedEmails = [];

    //display companies of the selected list
    let companiesTmp: any = [];
    element.companyRecipients.forEach(function(company) {
      companiesTmp.push(self.companies.filter(option => option.id === company));
    });
    self.selectedCompanies = companiesTmp.flat(1);

    //display emails of the selected list
    let emailTmp: any = [];
    element.emailRecipients.forEach(function(emailRecipient, i) {
      let domain = self.getDomain(emailRecipient);
      let index = self.checkDomain(emailTmp, domain);
      let email = { id: i, name: emailRecipient };

      if (index !== undefined) {
        emailTmp[index].emails.push(email);
      } else {
        emailTmp.push({
          domain: domain,
          emails: [email]
        });
      }
    });
    self.selectedEmails = emailTmp;

    //toggle recipients lists
    this.checkboxsList.forEach(function(CL, ind) {
      if (ind !== index) {
        self.checkboxsList[ind] = false;
      }
    });

    // empty fields if all recipients lists are unchecked
    if (this.checkboxsList.includes(true) === false) {
      this.selectedCompanies = [];
      this.selectedEmails = [];
    }
  }

  // remove all selected emails by domain
  removeAllSelected(domain) {
    this.selectedEmails = this.selectedEmails.filter(function(el) {
      return el.domain !== domain;
    });
  }

  // remove selected email/company
  removeSelected(ref, type) {
    switch (type) {
      case "email": {
        let domain = this.getDomain(ref);
        let index: any;
        this.selectedEmails = this.selectedEmails.filter(function(el, i) {
          index = i;
          if (el.domain === domain) {
            el.emails = el.emails.filter(function(elm) {
              return elm.name !== ref;
            });
          }
          return el.emails;
        });

        if (this.selectedEmails[index].emails.length === 0) {
          this.selectedEmails.splice(index, 1);
        }

        break;
      }
      case "company": {
        this.selectedCompanies = this.selectedCompanies.filter(function(el) {
          return el.id !== ref;
        });
        break;
      }
      default: {
        break;
      }
    }
  }

  // add the inserted email
  addRecipient() {
    if (this.searchRecipientsControl.value !== "") {
      let email = {
        id: "",
        name: this.searchRecipientsControl.value
      };
      let domain = this.getDomain(email.name);
      let index = this.checkDomain(this.selectedEmails, domain);

      if (index !== undefined) {
        if (
          this.selectedEmails[index].emails.filter(e => e.name === email.name)
            .length === 0
        ) {
          this.selectedEmails[index].emails.push(email);
        }
      } else {
        this.selectedEmails.push({
          domain: domain,
          emails: [email]
        });
      }

      this.hideAdd = false;
      this.searchRecipientsControl.patchValue("", { emitEvent: false });
    }
  }

  getDomain(email) {
    return email.substring(email.lastIndexOf("@") + 1);
  }

  checkDomain(array, domain) {
    let domainExist: any;
    array.forEach(function(SE, i) {
      if (SE.domain === domain) {
        domainExist = i;
      }
    });
    return domainExist;
  }

  validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  getUnique(arr, comp) {
    return arr.filter(el => !comp.includes(el));
  }

  arrayLength(arr) {
    let len = 0;
    arr.forEach(function(el) {
      len += el.emails.length;
    });
    return len;
  }
}
