import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { RecipientsComponent } from "./recipients/recipients.component";

const routes: Routes = [
  {
    path: "",
    component: RecipientsComponent,
    data: { title: "Recipients" }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(
      routes,
      {
        // anchorScrolling: "enabled",
        // scrollPositionRestoration: "enabled",
        //  useHash: true,
        //  enableTracing: false
      } // <-- debugging purposes only
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
